from django import forms

from .models import Diposit, Withdrawal, DepositProduct


class DepositForm(forms.ModelForm):
    class Meta:
        model = Diposit
        fields = ["amount"]


class DepositProductForm(forms.ModelForm):
    class Meta:
        model = DepositProduct

        fields = ["number_of_proc"]


class WithdrawalForm(forms.ModelForm):
    class Meta:
        model = Withdrawal
        fields = ["amount"]

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super(WithdrawalForm, self).__init__(*args, **kwargs)

    def clean_amount(self):
        amount = self.cleaned_data['amount']

        if self.user.account.balance < amount:
            raise forms.ValidationError(
                'You Can Not Withdraw More Than You Balance.'
            )

        return amount
