from django.core.management.base import BaseCommand
import datetime
import pymongo
from accounts.models import User


class Command(BaseCommand):

    def handle(self, *args, **options):
        mng_client = pymongo.MongoClient('localhost', 27017)
        mng_db = mng_client['local']

        collect = mng_db.get_collection("dailyss_report")
        users = User.objects.filter(account__balance__isnull=False)
        total_sum = 0.0
        for user in users:
            total_sum += float(user.balance)
        current_day=datetime.datetime.now().day
        collect.update_one({"date": current_day},
                                        {"$set":
                                        {"user_count": User.objects.count(),
                                         "total_amount": total_sum
                                         }}, upsert=True)
        for doc in collect.find():
            self.stdout.write(str(doc))

        self.stdout.write(self.style.SUCCESS('Successful'))
